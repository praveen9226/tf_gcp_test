# Package stage
#
FROM gcr.io/dataflow-templates-base/java11-template-launcher-base:latest
ADD . /app
WORKDIR /app
#ARG WORKDIR=/app/template
ARG MAINCLASS=EdrToKafkaLocalPipeline
#RUN mkdir -p ${WORKDIR}
#WORKDIR ${WORKDIR}



#RUN mkdir -p ${WORKDIR}/lib
#COPY --from=build /app/target/lib/ ${WORKDIR}/lib/



ENV FLEX_TEMPLATE_JAVA_CLASSPATH=EdrToKafkaPipeline-0.1.jar
ENV FLEX_TEMPLATE_JAVA_MAIN_CLASS=${MAINCLASS}
